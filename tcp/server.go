package tcp

import (
	"context"
	"my-go-redis/interface/tcp"
	"my-go-redis/lib/logger"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

type Config struct {
	Address string
}

func ListenAndServeWithSignal(cfg *Config, handler tcp.Handler) error {
	closeChan := make(chan struct{})
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		sig := <-sigChan
		switch sig {
		case syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT:
			closeChan <- struct{}{}

		}
	}()
	listener, err := net.Listen("tcp", cfg.Address)
	if err != nil {
		return err
	}
	logger.Info("start Listen")

	return ListenAndServe(listener, handler, closeChan)
}

func ListenAndServe(listener net.Listener,
	handler tcp.Handler,
	closeChan <-chan struct{}) error {

	go func() {
		<-closeChan
		logger.Info("shutting down")
		_ = listener.Close()
		_ = handler.Close()
	}()
	defer func() {
		_ = listener.Close()
		_ = handler.Close()
	}()

	ctx := context.Background()
	var waitDone sync.WaitGroup

	for true {
		conn, err := listener.Accept()
		if err != nil {
			return err
		}
		logger.Info("accepted link")
		//	开始服务 +1
		waitDone.Add(1)
		go func() {
			defer func() {
				//	服务完，-1
				waitDone.Done()
			}()
			handler.Handler(ctx, conn)

		}()
	}
	waitDone.Wait()
	return nil
}
