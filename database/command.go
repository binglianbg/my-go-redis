package database

import "strings"

// 记录该系统里所有的指令和command的关系
var cmdTable = make(map[string]*command)

type command struct {
	exector ExecFunc
	arity   int
}

func RegisterCommand(name string, exector ExecFunc, arity int) {
	name = strings.ToLower(name) //	统一转换为小写
	cmdTable[name] = &command{
		exector: exector,
		arity:   arity,
	}

}
