package aof

import (
	"my-go-redis/config"
	"my-go-redis/interface/database"
	"my-go-redis/lib/logger"
	"my-go-redis/lib/utils"
	"my-go-redis/resp/reply"
	"strconv"

	"os"
)

type CmdLine = [][]byte

const aofBufferSize = 1 << 16

type payload struct {
	cmdLine CmdLine
	dbIndex int
}

// 从通道里接受消息，并写入AOF文件中
type AofHandler struct {
	database database.Database
	//写aof文件的缓存池
	aofChain    chan *payload
	aofFile     *os.File //	.aof文件
	aofFilename string   //	文件名
	currentDB   int
}

// 创建aof，handler
func NewAOFHandler(database database.Database) (*AofHandler, error) {
	handler := &AofHandler{}
	handler.aofFilename = config.Properties.AppendFilename
	handler.database = database
	//	加载aof
	aofile, err := os.OpenFile(handler.aofFilename, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return nil, err
	}
	handler.aofFile = aofile
	//	channel缓冲
	handler.aofChain = make(chan *payload, aofBufferSize)
	go func() { handler.handleAof() }()
	return handler, nil
}

// Add payload(set k v) -> aofChan
func (handler *AofHandler) AddAof(dbIndex int, cmd CmdLine) {

	if config.Properties.AppendOnly && handler.aofChain != nil {
		handler.aofChain <- &payload{
			cmdLine: cmd,
			dbIndex: dbIndex,
		}
	}
}

// handleAof payload(set k v)	<- aofChan(落盘)
func (handler AofHandler) handleAof() {
	handler.currentDB = 0
	for p := range handler.aofChain {
		if p.dbIndex != handler.currentDB {
			data := reply.MakeMultiBulkReply(utils.ToCmdLine("SELECT", strconv.Itoa(p.dbIndex))).ToBytes()
			_, err := handler.aofFile.Write(data)
			if err != nil {
				logger.Error(err)
				continue // skip this command
			}
			handler.currentDB = p.dbIndex
		}
		data := reply.MakeMultiBulkReply(p.cmdLine).ToBytes()
		_, err := handler.aofFile.Write(data)
		if err != nil {
			logger.Warn(err)
		}
	}
}

// 加载aof
func (handler *AofHandler) LoadAof() {

}
